import _root_.sbt.Keys._

name := "CustomerChurnPrevention"

version := "1.0"

scalaVersion := "2.11.7"


libraryDependencies += "org.apache.spark" %% "spark-core" % "1.5.1"

libraryDependencies += "org.apache.spark" % "spark-mllib_2.11" % "1.5.1"

libraryDependencies += "com.databricks" % "spark-csv_2.10" % "1.2.0"

libraryDependencies += "joda-time" % "joda-time" % "2.9.2"
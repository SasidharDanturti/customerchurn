package CustomAggregateFunctions

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._
import org.joda.time.format.DateTimeFormat
import org.joda.time.{Weeks, Days, DateTime, DateTimeZone}

import scala.collection.Map

/**
  * Created by ragrawal on 9/23/15.
  * Computes Mean
  */

//Extend UserDefinedAggregateFunction to write custom aggregate function
//You can also specify any constructor arguments. For instance you
//can have CustomMean(arg1: Int, arg2: String)
class MostRecentPurchaseDate extends UserDefinedAggregateFunction {

  // Input Data Type Schema
  def inputSchema: StructType = StructType(Array(StructField("item", StringType)))

  // Intermediate Schema
  def bufferSchema = StructType(Array(
    StructField("cnt", StringType)
  ))

  // Returned Data Type .
  def dataType: DataType = StringType

  // Self-explaining
  def deterministic = true

  // This function is called whenever key changes
  def initialize(buffer: MutableAggregationBuffer) = {
    buffer(0) = "19000101"
  }

  // Iterate over each entry of a groffeup
  def update(buffer: MutableAggregationBuffer, input: Row) = {

   // DateTime.parse("20060401",DateTimeFormat.forPattern("yyyyMMdd"))
    val currentDate = DateTime.parse(input.getString(0),DateTimeFormat.forPattern("yyyyMMdd"))
    val oldDate = DateTime.parse( buffer(0).toString,DateTimeFormat.forPattern("yyyyMMdd"))

    if (oldDate.isBefore(currentDate)){
      buffer(0) = input.getString(0)
    }

  }

  // Merge two partial aggregates
  def merge(buffer1: MutableAggregationBuffer, buffer2: Row) = {

    val currentDate = DateTime.parse(buffer1(0).toString,DateTimeFormat.forPattern("yyyyMMdd"))
    val oldDate = DateTime.parse( buffer2(0).toString,DateTimeFormat.forPattern("yyyyMMdd"))

    if (oldDate.isAfter(currentDate)){
      buffer1(0) = buffer2(0)
    }

  }

  // Called after all the entries are exhausted.
  def evaluate(buffer: Row) = {
    buffer.getString(0)
  }

}

package CustomAggregateFunctions

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

import scala.collection.Map

/**
  * Created by ragrawal on 9/23/15.
  * Computes Mean
  */

//Extend UserDefinedAggregateFunction to write custom aggregate function
//You can also specify any constructor arguments. For instance you
//can have CustomMean(arg1: Int, arg2: String)
class Frequency extends UserDefinedAggregateFunction {

  // Input Data Type Schema
  def inputSchema: StructType = StructType(Array(StructField("item", StringType)))

  // Intermediate Schema
  def bufferSchema = StructType(Array(
    StructField("cnt", MapType(StringType,IntegerType))
  ))

  // Returned Data Type .
  def dataType: DataType = StringType

  // Self-explaining
  def deterministic = true

  // This function is called whenever key changes
  def initialize(buffer: MutableAggregationBuffer) = {
    buffer(0) = scala.collection.mutable.HashMap.empty[String,Int]// set number of items to 0
  }

  // Iterate over each entry of a groffeup
  def update(buffer: MutableAggregationBuffer, input: Row) = {

    val currentKey = input.getString(0)

    if(buffer.getMap[String,Int](0).keySet.exists(_ ==currentKey.toString)){
      val count = buffer.getMap[String,Int](0).get(currentKey.toString).get + 1
      val currentMap: Map[String, Int] = buffer.getMap[String, Int](0)
      val newMap = currentMap + (currentKey.toString -> count)
      buffer(0) = newMap

    }else{
      val currentMap: Map[String, Int] = buffer.getMap[String, Int](0)
      val newMap = currentMap + (currentKey.toString -> 1)
      buffer(0) = newMap
    }

  }

  // Merge two partial aggregates
  def merge(buffer1: MutableAggregationBuffer, buffer2: Row) = {

    buffer1(0) = buffer1.getMap[String, Int](0) ++ buffer2.getMap[String, Int](0)
  }

  // Called after all the entries are exhausted.
  def evaluate(buffer: Row) = {

    getFrequencyScore( buffer.getMap[String, Int](0).keys.size).toString
  }

  def getFrequencyScore(frequency: Long): Int ={

   if(frequency > 10){
     return 5
   }else if(frequency <=10 & frequency > 7){
     return 4
   }else if(frequency <=7 & frequency > 5){
     return 3
   }else if(frequency <=5 & frequency > 3){
     return 2
   } else{
     return 1
   }

  }

}
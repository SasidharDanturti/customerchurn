import CustomAggregateFunctions.{Recency, Frequency, MostRecentPurchaseDate, Mode}
import breeze.linalg.Options.Median
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.functions._
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, GroupedData, DataFrame, SQLImplicits}
import org.apache.spark.sql.functions.{concat, lit}


/**
  * Created by synerzip on 11/2/16.
  */
object RFMCalculator extends App {
  val conf = new SparkConf().setMaster("local").setAppName("ChurnPrevention")
  val sc = new SparkContext(conf)
  val sqlContext = new org.apache.spark.sql.SQLContext(sc)


  val convertToDouble:(String => Double) = {
    case b => b.toDouble
  }

  val calculateMonetaryValue:(Double=> String) = {
    case b => {
      getMonetaryScore(b).toString
    }
  }

//  val getConcatenated = udf( (first: String, second: String,third:String) => { first +  second + third } )


  def getMonetaryScore(frequency:Double):Int={

    if(frequency > 400){
      return 5
    }else if(frequency <=400 & frequency > 300){
      return 4
    }else if(frequency <=300 & frequency > 100){
      return 3
    }else if(frequency <=100 & frequency > 50){
      return 2
    } else{
      return 1
    }

  }

//  val toVec4    = udf[Vector, Int, Int, String, String] { (a,b,c,d) =>
//    val e3 = c match {
//      case "hs-grad" => 0
//      case "bachelors" => 1
//      case "masters" => 2
//    }
//    val e4 = d match {case "male" => 0 case "female" => 1}
//    Vectors.dense(a, b, e3, e4)
//  }
//


  val udfToDouble = udf(convertToDouble)
  val udfToMonterayScore = udf(calculateMonetaryValue)


  val customerData = getCSVData("/home/synerzip/Sasidhar/Research/Customer Segmentation/Data/CustomerData/transactions_200607.csv")

//  val newData=customerData.filter("CUST_CODE ='CUST0000000068'")
//            .withColumn("SPEND",udfToDouble(col("SPEND"))) //  private val groupedData: GroupedData = customerData.groupBy("CUST_CODE")
//            .withColumn("QUANTITY",udfToDouble(col("QUANTITY")))
//
//
//  val customMode = new CustomMode()
////  val recentDate = new MostRecentPurchaseDate()
//  val frequency = new Frequency()
//  val recency = new Recency()
//
//  val newData1 = newData.groupBy("CUST_CODE").agg(avg("SPEND").as("Avg Spend"),
//    recency(newData.col("SHOP_DATE")).as("RecencyScore"),
//    frequency(newData.col("BASKET_ID")).as("FrequencyScore"))
//
//  val newData2 = newData1.withColumn("MonetaryScore",udfToMonterayScore(col("Avg Spend")))
//
//  newData2.registerTempTable("newData2")
//  val newData3 = sqlContext.sql("select *,CONCAT(RecencyScore,FrequencyScore,MonetaryScore) as RFM from newData2")


  val customMode = new Mode()
  //  val recentDate = new MostRecentPurchaseDate()
  val frequency = new Frequency()
  val recency = new Recency()

  val aggregatedData=(customerData
    .withColumn("SPEND",udfToDouble(col("SPEND"))) //  private val groupedData: GroupedData = customerData.groupBy("CUST_CODE")
    .groupBy("CUST_CODE").agg(avg("SPEND").as("Avg Spend"),
    recency(customerData.col("SHOP_DATE")).as("RecencyScore"),
    frequency(customerData.col("BASKET_ID")).as("FrequencyScore"))).withColumn("MonetaryScore",udfToMonterayScore(col("Avg Spend")))


//  val newData1 = newData.groupBy("CUST_CODE").agg(avg("SPEND").as("Avg Spend"),
//    recency(newData.col("SHOP_DATE")).as("RecencyScore"),
//    frequency(newData.col("BASKET_ID")).as("FrequencyScore"))
//
//  val newData2 = newData1.withColumn("MonetaryScore",udfToMonterayScore(col("Avg Spend")))

  aggregatedData.registerTempTable("newData")
  val rfmData = sqlContext.sql("select *,CONCAT(RecencyScore,FrequencyScore,MonetaryScore) as RFM from newData")

  rfmData.show()

  def getCSVData(filePath:String):DataFrame={

    var data = sqlContext.read.format("com.databricks.spark.csv").options(Map("header"-> "true","inferSchema"->"true")).load(filePath)
    data
    //data.take(100)
//    data.filter("CUST_CODE ='CUST0000000031' or CUST_CODE ='CUST0000020724' ")
  }


}

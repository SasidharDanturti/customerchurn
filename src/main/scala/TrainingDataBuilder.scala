import CustomAggregateFunctions._
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.{PipelineModel, Pipeline}
import org.apache.spark.ml.regression.RandomForestRegressor
import org.apache.spark.mllib.linalg.{Vectors, Vector}
import org.apache.spark.sql.functions._
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.DataFrame
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.util.Random

import org.apache.spark.ml.feature.{IndexToString, VectorAssembler, OneHotEncoder, StringIndexer}
/**
  * Created by synerzip on 23/3/16.
  */
object TrainingDataBuilder extends App {

  val conf = new SparkConf().setMaster("local").setAppName("ChurnPrevention")
  val sc = new SparkContext(conf)
  val sqlContext = new org.apache.spark.sql.SQLContext(sc)

  getLabelledData

  def getLabelledData:DataFrame = {
    val customerData: DataFrame = getCustomerData("/home/synerzip/Sasidhar/Research/Customer Segmentation/Data/CustomerData/transactions_200607.csv")
    val labelledCustomers = labelCustomersForChurn(customerData)
    val model = generateModel(labelledCustomers.filter("Churned=true"))

    calculateCLV(labelledCustomers.filter("Churned=false"),model)

    labelledCustomers
  }

  def calculateCLV(unChurnedCustomers:DataFrame,model:PipelineModel) ={
    //predictTheLifeTimeOfExistingCustomers(labelledCustomers)
    val predictions = model.transform(unChurnedCustomers)
    predictions.registerTempTable("predictions")
    val clvData = sqlContext.sql("select CUST_CODE,(Avg_Spend * FrequencyScore * prediction)  as CLV from predictions")

    clvData.show()
    predictions.show()
  }

//  def predictTheLifeTimeOfExistingCustomers(labelledCustomers:DataFrame) : DataFrame ={
//    val dataWithFeatures = generateFeaturesOutOfTheData(labelledCustomers)
//    dataWithFeatures
//  }

  def generateModel(labelledCustomers:DataFrame) : PipelineModel ={

    val Array(trainingData, testData) = labelledCustomers.randomSplit(Array(0.7, 0.3))
      val dfFilled = trainingData
//    val vecotorize: ((String,String,String,String,String,String) => Vector)= {
//      case a => Vectors.dense(a._1.toDouble,a._2.toDouble,a._3.toDouble,a._4.toDouble,a._5.toDouble,a._6.toDouble)
//    }
//
//    val encodeLabel:(String => Double) = {
//      case "1" => 1.0
//      case "2" => 2.0
//    }
//    val udfVecotorize = udf(vecotorize)
//    val udfEncodeLabel = udf(encodeLabel)
//
//    val indexer = new StringIndexer()
//      .setInputCol("CUST_PRICE_SENSITIVITY")
//      .setOutputCol("categoryIndex")
//      .fit(labelledCustomers)
//    val indexed = indexer.transform(labelledCustomers)
//
//    val encoder = new OneHotEncoder()
//      .setInputCol("categoryIndex")
//      .setOutputCol("categoryVec")
//
//    val encoded = encoder.transform(indexed)
//    //encoded.select("id", "categoryVec").show()
//    encoded.show()

//    val newData = cleandata.withColumn("features",udfVecotorize(col("Pclass"),col("Sex"),col("Age"),col("SibSp"),col("Fare"),col("Embarked")))
//      .withColumn("label",(col("Survived"))).select("label","features")
//    newData.show
//    newData.printSchema()
//    newData



    val categoricalFeatColNames = Seq("CUST_PRICE_SENSITIVITY", "CUST_LIFESTAGE","BASKET_SIZE","RecencyScore","FrequencyScore",
                                      "BASKET_PRICE_SENSITIVITY","BASKET_TYPE","BASKET_DOMINANT_MISSION")

    val stringIndexers = categoricalFeatColNames.map { colName =>
      new StringIndexer()
        .setInputCol(colName)
        .setOutputCol(colName + "Indexed")
        .fit(dfFilled)
    }

    val labelIndexer = new StringIndexer()
      .setInputCol("CUSTOMER_SINCE")
      .setOutputCol("SurvivedIndexed")
      .fit(dfFilled)

    val numericFeatColNames = Seq("Avg_Spend")

    val idxdCategoricalFeatColName = categoricalFeatColNames.map(_ + "Indexed")

    val allIdxdFeatColNames = numericFeatColNames ++ idxdCategoricalFeatColName

    val assembler = new VectorAssembler()
      .setInputCols(Array(allIdxdFeatColNames: _*))
      .setOutputCol("Features")


    val randomForest = new RandomForestRegressor()
      .setLabelCol("SurvivedIndexed")
      .setFeaturesCol("Features")

    val labelConverter = new IndexToString()
      .setInputCol("prediction")
      .setOutputCol("predictedLabel")
      .setLabels(labelIndexer.labels)

    val pipeline = new Pipeline().setStages(Array.concat(
      stringIndexers.toArray,
      Array(labelIndexer, assembler, randomForest, labelConverter)
    ))

    val model = pipeline.fit(dfFilled)


    val predictions = model.transform(testData)

    // Select example rows to display.
    //predictions.select("prediction", "label", "features").show(5)

    // Select (prediction, true label) and compute test error
    val evaluator = new RegressionEvaluator()
      .setLabelCol("SurvivedIndexed")
      .setPredictionCol("prediction")
      .setMetricName("rmse")
    val rmse = evaluator.evaluate(predictions)
    println("Root Mean Squared Error (RMSE) on test data = " + rmse)

    model

  }





  def getCustomerData(filePath: String): DataFrame = {

    val data = sqlContext.read.format("com.databricks.spark.csv").options(Map("header" -> "true", "inferSchema" -> "true")).load(filePath)
    data
    //data.take(100)
    //    data.filter("CUST_CODE ='CUST0000000031' or CUST_CODE ='CUST0000020724' ")
  }

  def labelCustomersForChurn(customerData1: DataFrame): DataFrame = {

    def getMonetaryScore(frequency:Double):Int={

      if(frequency > 400){
        return 5
      }else if(frequency <=400 & frequency > 300){
        return 4
      }else if(frequency <=300 & frequency > 100){
        return 3
      }else if(frequency <=100 & frequency > 50){
        return 2
      } else{
        return 1
      }

    }
    

    val calculateMonetaryValue:(Double=> String) = {
      case b => {
        getMonetaryScore(b).toString
      }
    }

    //  val getConcatenated = udf( (first: String, second: String,third:String) => { first +  second + third } )


    def getChurnLabel(recentDate: String): Boolean = {
      val thresholdDate = DateTime.parse("20060414",DateTimeFormat.forPattern("yyyyMMdd"))
      val mostRecentDate = DateTime.parse( recentDate,DateTimeFormat.forPattern("yyyyMMdd"))

      if (mostRecentDate.isBefore(thresholdDate)){
        return true
      }
      return false
    }

    val labelChurn: (String => Boolean) = {
      case b => {
        getChurnLabel(b)
      }
    }

    val randUdf = udf({() => Random.nextInt(7)+1})
    //df.selectPlus(randUdf() as "vrand")


    val udfLabelChurn = udf(labelChurn)

    val mode = new Mode()
    val recentDate = new MostRecentPurchaseDate()
    val frequency = new FrequencyValue()
    val recency = new Recency()

//    val customerData = customerData1.withColumn("CUSTOMER_SINCE",randn(10).alias("uniform"))

    val customerData = customerData1

    val aggregatedData = (customerData.filter("CUST_CODE !=''").groupBy("CUST_CODE").agg(avg("SPEND").as("Avg_Spend"),
      recentDate(customerData.col("SHOP_DATE")).as("RecentDate"),
      mode(customerData.col("CUST_PRICE_SENSITIVITY")).as("CUST_PRICE_SENSITIVITY"),
      mode(customerData.col("CUST_LIFESTAGE")).as("CUST_LIFESTAGE"),
      mode(customerData.col("BASKET_SIZE")).as("BASKET_SIZE"),
      mode(customerData.col("BASKET_PRICE_SENSITIVITY")).as("BASKET_PRICE_SENSITIVITY"),
      mode(customerData.col("BASKET_TYPE")).as("BASKET_TYPE"),
      mode(customerData.col("BASKET_DOMINANT_MISSION"))as("BASKET_DOMINANT_MISSION"),
      //,mode(customerData.col("CUSTOMER_SINCE"))
      recency(customerData.col("SHOP_DATE")).as("RecencyScore"),
      frequency(customerData.col("BASKET_ID")).as("FrequencyScore")

    )).withColumn("CUSTOMER_SINCE",randUdf())


//    aggregatedData.show()
    val labelledData = aggregatedData.withColumn("Churned", udfLabelChurn(col("RecentDate")))
    //labelledData.show(500)
    labelledData
  }

}
